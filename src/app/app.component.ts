import { Component, OnInit } from '@angular/core';
import { AuthGuard } from './service/auth.service';
import { AngularFire } from 'angularfire2';

import * as firebase from 'firebase';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  isAuthenticated = false;
  
  constructor(private authService: AuthGuard,
  private af: AngularFire) {}
  
  ngOnInit(){
    this.authService.canActivate().subscribe(
      authStatus => {
        this.isAuthenticated = authStatus;
        //console.log(this.isAuthenticated)
      });
      console.log(this.af);
      console.log(firebase);
     // console.log(this.isAuth());
     
     
    /*
      Very import
      GET USER
    */
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        console.log(user);
      } else {
        // No user is signed in.
      }
    });
    
    // console.log(firebase.auth());
    
  }
  
  isAuth() {
    return this.isAuthenticated;
  }
  


  
  
}
