import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import * as firebase from 'firebase';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmailComponent } from './email/email.component';
import { SignupComponent } from './signup/signup.component';
import { MembersComponent } from './members/members.component';

import { AuthGuard } from './service/auth.service';
import { GamelistService } from './service/gamelist.service';
import { GroupService } from './service/group.service';
import { ProfileService } from './service/profile.service';

import { routes } from './app.routes';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { HeaderComponent } from './header/header.component';
import { FrontpageComponent } from './frontpage/frontpage.component';
import { ProfilecreateComponent } from './profilecreate/profilecreate.component';
import { GamelistComponent } from './gamelist/gamelist.component';
import { GamedetailComponent } from './gamedetail/gamedetail.component';
import { GroupingComponent } from './grouping/grouping.component';

export const firebaseConfig = {
    apiKey: "AIzaSyACQNfVzTxrDUaqQMS7MyPc2z5Bl8En12w",
    authDomain: "matchgame-f33fe.firebaseapp.com",
    databaseURL: "https://matchgame-f33fe.firebaseio.com",
    storageBucket: "matchgame-f33fe.appspot.com",
    messagingSenderId: "800222234508"
};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmailComponent,
    SignupComponent,
    MembersComponent,
    ForgotPasswordComponent,
    HeaderComponent,
    FrontpageComponent,
    ProfilecreateComponent,
    GamelistComponent,
    GamedetailComponent,
    GroupingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routes
  ],
  providers: [AuthGuard, GamelistService, GroupService, ProfileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
