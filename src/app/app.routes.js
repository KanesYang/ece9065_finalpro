"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require('./login/login.component');
var forgot_password_component_1 = require('./forgot-password/forgot-password.component');
var members_component_1 = require('./members/members.component');
var auth_service_1 = require('./service/auth.service');
var signup_component_1 = require('./signup/signup.component');
var email_component_1 = require('./email/email.component');
var frontpage_component_1 = require('./frontpage/frontpage.component');
var profilecreate_component_1 = require('./profilecreate/profilecreate.component');
var gamelist_component_1 = require('./gamelist/gamelist.component');
var gamedetail_component_1 = require('./gamedetail/gamedetail.component');
var grouping_component_1 = require('./grouping/grouping.component');
exports.router = [
    { path: '', redirectTo: 'frontpage', pathMatch: 'full' },
    { path: 'frontpage', component: frontpage_component_1.FrontpageComponent },
    { path: 'login', component: login_component_1.LoginComponent },
    { path: 'forgot', component: forgot_password_component_1.ForgotPasswordComponent },
    { path: 'signup', component: signup_component_1.SignupComponent },
    { path: 'login-email', component: email_component_1.EmailComponent },
    { path: 'gamelist', component: gamelist_component_1.GamelistComponent },
    { path: 'lookingforgroup', component: grouping_component_1.GroupingComponent },
    { path: 'gamedetail/:id', component: gamedetail_component_1.GamedetailComponent },
    { path: 'members', component: members_component_1.MembersComponent, canActivate: [auth_service_1.AuthGuard] },
    { path: 'profilecreate', component: profilecreate_component_1.ProfilecreateComponent, canActivate: [auth_service_1.AuthGuard] }
];
exports.routes = router_1.RouterModule.forRoot(exports.router);
