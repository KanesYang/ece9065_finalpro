import { Component, Input, OnInit  } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

import { AuthGuard } from '../service/auth.service';
import { GamelistService } from '../service/gamelist.service';

import { Listgame } from '../model/Listgame';
import { ListPlayer } from '../model/ListPlayer';

@Component({
  selector: 'app-gamedetail',
  templateUrl: './gamedetail.component.html',
  styleUrls: ['./gamedetail.component.css']
})
export class GamedetailComponent implements OnInit {
  
  //new 
  uid: string;

  constructor(private gamelistService: GamelistService,
              private route: ActivatedRoute,
	 			      private location: Location,
	 			      private authService: AuthGuard,
	 			      private af: AngularFire) {

	 			        
    this.authService.canActivate().subscribe(
      authStatus => {
        this.isAuthenticated = authStatus;
        console.log(this.isAuthenticated)
      });
      

	}
	 	
	ngOnInit() {
    
    //get the uid
    //IMPORTANT 	 			        
	 	this.uid = this.authService.getUid();
    console.log(this.uid);
    
    //get the current user
    this.currentUser = this.af.database.list('/users/user'+this.uid+'/gameinfo');
    
     this.currentUser.subscribe(
        user => {
          //this.currentUserName = user.infoGameId + "(" + user.infoGamePlatform + ")";
          console.log(user)}
    )
    
    //put the game info into the page from the service
    this.game = this.gamelistService.getGame(this.route.snapshot.params.id);
    //console.log(this.game)
    //get the gameid
    this.gameid = this.route.snapshot.params.id;
    //console.log(this.gameid);
    
    //get the player list
    this.playerList = this.gamelistService.getPlayerList(this.gameid);
    this.playerList.subscribe(
        list => this.checkList = list
      )
    
  }
  //end of ngOnInit
	 	
  @Input() game;
  
  gameid;
  //current user's gameinfo
  currentUser: FirebaseListObservable<any>;
  currentList;
  
  currentUserName: string;
  
  //content
  playerList: FirebaseListObservable<any>;
  checkList: ListPlayer[];
  isAdded;
  
  
  //whether the user is login or not
  isAuthenticated;
  
  isAuth() {
    return this.isAuthenticated;
  }
  
  //end of auth
  
  
  
  //add user to the player list
  onAddPlayer(newPlayerInfo) {
    let newPlayer = {
      uid: this.uid,
      name: newPlayerInfo
    }
    console.log(newPlayer);
    console.log(this.checkName(newPlayer.uid));
    //refuse when the select box is blank
    if(newPlayer.name === ""){
      alert("this user info is blank, you cannot add this!, go to the profile page to create your own personal profile of this game first!")
    } else {
      //judge whether the user is added or not
      if( this.checkName(newPlayer.uid).length != 0 ){
        alert("You have added yourself!");
        this.isAdded = true;
      } else {
        console.log("success");
        this.isAdded = true;
        return this.gamelistService.addPlayer(newPlayer);
      }
    }
  }
  
  //check whether the use is on the list
  checkName(userid) {
    let added = this.checkList.filter( (item:ListPlayer) => item.uid === userid) 
    return added;
  }

}
