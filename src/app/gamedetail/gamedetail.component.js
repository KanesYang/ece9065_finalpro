"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/switchMap');
var GamedetailComponent = (function () {
    function GamedetailComponent(gamelistService, route, location, authService, af) {
        var _this = this;
        this.gamelistService = gamelistService;
        this.route = route;
        this.location = location;
        this.authService = authService;
        this.af = af;
        this.authService.canActivate().subscribe(function (authStatus) {
            _this.isAuthenticated = authStatus;
            console.log(_this.isAuthenticated);
        });
    }
    GamedetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        //get the uid
        //IMPORTANT 	 			        
        this.uid = this.authService.getUid();
        console.log(this.uid);
        //get the current user
        this.currentUser = this.af.database.list('/users/user' + this.uid + '/gameinfo');
        this.currentUser.subscribe(function (user) {
            //this.currentUserName = user.infoGameId + "(" + user.infoGamePlatform + ")";
            console.log(user);
        });
        //put the game info into the page from the service
        this.game = this.gamelistService.getGame(this.route.snapshot.params.id);
        //console.log(this.game)
        //get the gameid
        this.gameid = this.route.snapshot.params.id;
        //console.log(this.gameid);
        //get the player list
        this.playerList = this.gamelistService.getPlayerList(this.gameid);
        this.playerList.subscribe(function (list) { return _this.checkList = list; });
    };
    GamedetailComponent.prototype.isAuth = function () {
        return this.isAuthenticated;
    };
    //end of auth
    //add user to the player list
    GamedetailComponent.prototype.onAddPlayer = function (newPlayerInfo) {
        var newPlayer = {
            uid: this.uid,
            name: newPlayerInfo
        };
        console.log(newPlayer);
        console.log(this.checkName(newPlayer.uid));
        //refuse when the select box is blank
        if (newPlayer.name === "") {
            alert("this user info is blank, you cannot add this!, go to the profile page to create your own personal profile of this game first!");
        }
        else {
            //judge whether the user is added or not
            if (this.checkName(newPlayer.uid).length != 0) {
                alert("You have added yourself!");
                this.isAdded = true;
            }
            else {
                console.log("success");
                this.isAdded = true;
                return this.gamelistService.addPlayer(newPlayer);
            }
        }
    };
    //check whether the use is on the list
    GamedetailComponent.prototype.checkName = function (userid) {
        var added = this.checkList.filter(function (item) { return item.uid === userid; });
        return added;
    };
    __decorate([
        core_1.Input()
    ], GamedetailComponent.prototype, "game");
    GamedetailComponent = __decorate([
        core_1.Component({
            selector: 'app-gamedetail',
            templateUrl: './gamedetail.component.html',
            styleUrls: ['./gamedetail.component.css']
        })
    ], GamedetailComponent);
    return GamedetailComponent;
}());
exports.GamedetailComponent = GamedetailComponent;
