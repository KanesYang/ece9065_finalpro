import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable } from 'angularfire2';
import { Router } from '@angular/router';

import * as firebase from 'firebase';

@Component({
  selector: 'app-frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.css']
})
export class FrontpageComponent implements OnInit {

  constructor(private af: AngularFire) { }
  
  userCount: FirebaseListObservable<any[]>;
  
  ngOnInit() {
    this.userCount = this.af.database.list('/users');
    console.log(this.userCount);
  }

}
