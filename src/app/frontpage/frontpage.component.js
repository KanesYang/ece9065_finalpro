"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var FrontpageComponent = (function () {
    function FrontpageComponent(af) {
        this.af = af;
    }
    FrontpageComponent.prototype.ngOnInit = function () {
        this.userCount = this.af.database.list('/users');
        console.log(this.userCount);
    };
    FrontpageComponent = __decorate([
        core_1.Component({
            selector: 'app-frontpage',
            templateUrl: './frontpage.component.html',
            styleUrls: ['./frontpage.component.css']
        })
    ], FrontpageComponent);
    return FrontpageComponent;
}());
exports.FrontpageComponent = FrontpageComponent;
