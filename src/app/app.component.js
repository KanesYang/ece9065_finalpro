"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var firebase = require('firebase');
var AppComponent = (function () {
    function AppComponent(authService, af) {
        this.authService = authService;
        this.af = af;
        this.title = 'app works!';
        this.isAuthenticated = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.canActivate().subscribe(function (authStatus) {
            _this.isAuthenticated = authStatus;
            //console.log(this.isAuthenticated)
        });
        console.log(this.af);
        console.log(firebase);
        // console.log(this.isAuth());
        /*
          Very import
          GET USER
        */
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                // User is signed in.
                console.log(user);
            }
            else {
            }
        });
        // console.log(firebase.auth());
    };
    AppComponent.prototype.isAuth = function () {
        return this.isAuthenticated;
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
