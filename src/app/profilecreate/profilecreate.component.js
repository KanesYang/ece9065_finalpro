"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/switchMap');
require('rxjs/add/operator/share');
var firebase = require('firebase');
var ProfilecreateComponent = (function () {
    function ProfilecreateComponent(authService, profileService, af) {
        var _this = this;
        this.authService = authService;
        this.profileService = profileService;
        this.af = af;
        //
        // part 3 for the usergame part
        //
        this.platforms = [
            {
                name: 'PC',
                games: [
                    {
                        name: 'Left 4 Dead 2',
                        character: ['Coach', 'Rochell', 'Nick', 'Ellis']
                    },
                    {
                        name: 'World of Warcraft',
                        character: ['Warrior', 'Paladin', 'Hunter', 'Rogue', 'Priest', 'Death Knight', 'haman', 'Mage', 'Warlock', 'Monk', 'Druid', 'Demon Hunter']
                    }
                ]
            },
            {
                name: 'PS3',
                games: [
                    {
                        name: 'Destiny',
                        character: ['Hunter', 'Warlock', 'Titan']
                    },
                    {
                        name: 'Diablo 3',
                        character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
                    }
                ]
            },
            {
                name: 'PS4',
                games: [
                    {
                        name: 'Destiny',
                        character: ['Hunter', 'Warlock', 'Titan']
                    },
                    {
                        name: 'Diablo 3',
                        character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
                    }
                ]
            },
            {
                name: 'XBox',
                games: [
                    {
                        name: 'Destiny',
                        character: ['Hunter', 'Warlock', 'Titan']
                    },
                    {
                        name: 'Diablo 3',
                        character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
                    },
                    {
                        name: 'Left 4 Dead 2',
                        character: ['Coach', 'Rochell', 'Nick', 'Ellis']
                    }
                ]
            },
            {
                name: 'XB1',
                games: [
                    {
                        name: 'Destiny',
                        character: ['Hunter', 'Warlock', 'Titan']
                    },
                    {
                        name: 'Diablo 3',
                        character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
                    }
                ]
            }
        ];
        this.wowCharacters = ['Warrior', 'Paladin', 'Hunter', 'Rogue', 'Priest', 'Death Knight', 'haman', 'Mage', 'Warlock', 'Monk', 'Druid', 'Demon Hunter'];
        this.destinyCharacters = ['Hunter', 'Warlock', 'Titan'];
        this.D3Characters = ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard'];
        this.L4D2Characters = ['Coach', 'Rochell', 'Nick', 'Ellis'];
        this.uid = this.authService.getUid();
        console.log(this.uid);
        /*
          basic profile + show image
        */
        this.currentBasicProfile = this.profileService.getBasicProfile(this.uid);
        console.log(this.currentBasicProfile);
        this.profileService.getBasicProfile(this.uid).subscribe(function (basic) {
            _this.basicObject = basic;
            //console.log(this.basicObject.path);
            var storageRef = firebase.storage().ref();
            var spaceRef = storageRef.child(_this.basicObject.path);
            storageRef.child(_this.basicObject.path).getDownloadURL().then(function (url) {
                _this.imageUrl = url;
            }).catch(function (error) {
                console.log(error);
            });
        });
        /*
        End of  basic profile + show image
        */
        /*
          start of platform profile
        */
        /*
          start of game profile
        */
        //game profile
        this.currentGameProfile = this.profileService.getGameProfile(this.uid);
        console.log(this.currentBasicProfile);
        //character
        this.infoGameCharacter = [];
    }
    ProfilecreateComponent.prototype.ngOnInit = function () {
    };
    //part for restrict the file size
    //submit the form
    ProfilecreateComponent.prototype.onSubmitProfileBasic = function () {
        //check the age
        if (this.age > 110 || this.age < 1) {
            alert("The age should between 1 and 110!!");
            return false;
        }
        var basicProfile = {
            uid: this.uid,
            username: this.username,
            age: this.age,
            startTime: this.startTime,
            endTime: this.endTime,
            image: this.image
        };
        this.profileService.addBasicProfile(basicProfile);
    };
    ProfilecreateComponent.prototype.onSubmitProfileGame = function () {
        var gameProfile = {
            uid: this.uid,
            infoGamePlatform: this.infoGamePlatform,
            infoGameName: this.infoGameName,
            infoGameCharacter: this.infoGameCharacter,
            infoGameStyle: this.infoGameStyle,
            infoGameId: this.infoGameId,
            infoChannel: this.infoChannel
        };
        this.profileService.addGameProfile(gameProfile);
        this.infoGameCharacter = [];
    };
    //
    // part 2 for the character part
    // use those line of code to grab the multicheck box
    //
    ProfilecreateComponent.prototype.updateCheckedOptions = function (character, event) {
        var index = this.infoGameCharacter.indexOf(character);
        console.log(this.infoGameCharacter);
        if (event.target.checked) {
            if (index < 0) {
                this.infoGameCharacter.push(character);
                console.log(character);
            }
        }
        else {
            if (index >= 0) {
                this.infoGameCharacter.splice(index, 1);
                console.log(index);
            }
        }
    };
    ProfilecreateComponent.prototype.onSelectPlatform = function (platform) {
        this.selectedPlatform = platform;
        console.log(this.selectedPlatform);
    };
    ProfilecreateComponent.prototype.onSelectGame = function (game) {
        this.selectedGame = game;
        console.log(this.selectedGame);
    };
    ProfilecreateComponent = __decorate([
        core_1.Component({
            selector: 'app-profilecreate',
            templateUrl: './profilecreate.component.html',
            styleUrls: ['./profilecreate.component.css']
        })
    ], ProfilecreateComponent);
    return ProfilecreateComponent;
}());
exports.ProfilecreateComponent = ProfilecreateComponent;
