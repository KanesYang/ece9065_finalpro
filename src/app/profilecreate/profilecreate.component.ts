import { Component, Input, OnInit  } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/share';

import * as firebase from 'firebase';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

import { AuthGuard } from '../service/auth.service';
import { ProfileService } from '../service/profile.service';

import { UserBasic } from '../model/UserBasic';
import { UserPlatform } from '../model/UserPlatform';
import { UserGame } from '../model/UserGame';

import { Platform } from '../model/Platform';
import { Game } from '../model/Game';

@Component({
  selector: 'app-profilecreate',
  templateUrl: './profilecreate.component.html',
  styleUrls: ['./profilecreate.component.css']
})
export class ProfilecreateComponent implements OnInit {
  
  constructor(
      private authService: AuthGuard,
      private profileService: ProfileService,
      private af: AngularFire
    ) 
    {
      
      this.uid = this.authService.getUid();      
      console.log(this.uid);
      
      /*
        basic profile + show image
      */

      this.currentBasicProfile = this.profileService.getBasicProfile(this.uid);
      console.log(this.currentBasicProfile);
      
      this.profileService.getBasicProfile(this.uid).subscribe(
          basic => {
            this.basicObject = basic;
            //console.log(this.basicObject.path);
            let storageRef = firebase.storage().ref();
            let spaceRef = storageRef.child(this.basicObject.path);
            storageRef.child(this.basicObject.path).getDownloadURL().then(
                url => {
                    this.imageUrl = url;                
                }).catch((error) => {
                    console.log(error);
                });
          })
    /*
    End of  basic profile + show image
    */
    
    /*
      start of platform profile 
    */
     

    /*
      start of game profile 
    */
     //game profile
     
     this.currentGameProfile = this.profileService.getGameProfile(this.uid);
     console.log(this.currentBasicProfile);
     
     //character
     this.infoGameCharacter = [];
     
    }
    

    
  ngOnInit() {

  }
  /*
  User Basic 
  */
  basicObject: any;
  currentBasicProfile: FirebaseObjectObservable<any>;
  
  imageUrl: string;
  uid: string;
  image: any;
  username: string;
  age: number;
  
  timeZone: string;
  
  weekdayTime: any;
  weekendTime: any;
  
  //part for restrict the file size
  
  //submit the form
  onSubmitProfileBasic(){
    
    //check the age
    if(this.age > 110 || this.age < 1) {
      alert("The age should between 1 and 110!!")
      return false;
    }
    
    let basicProfile = {
      uid: this.uid,
      username: this.username,
      age: this.age,
      timeZone: this.timeZone,
      weekdayTime: this.weekdayTime,
      weekendTime: this.weekendTime,
      image: this.image
    }
    
    this.profileService.addBasicProfile(basicProfile);
  }
  

  //
  // part 1 for the usergame part
  //usergame part basic part
  
  currentGameProfile: FirebaseListObservable<any[]>;
  
  infoGamePlatform: string;
  infoGameName: string;
  infoGameCharacter: string[];
  infoGameStyle: string;
  infoGameId: string;
  infoChannel: boolean;
  
  onSubmitProfileGame() {
    let gameProfile = {
      uid: this.uid,
      infoGamePlatform: this.infoGamePlatform,
      infoGameName: this.infoGameName,
      infoGameCharacter: this.infoGameCharacter,
      infoGameStyle: this.infoGameStyle,
      infoGameId: this.infoGameId,
      infoChannel: this.infoChannel
    }
    
    this.profileService.addGameProfile(gameProfile);
    this.infoGameCharacter = [];
  }
  
  //
  // part 2 for the character part
  // use those line of code to grab the multicheck box
  //
  updateCheckedOptions(character, event) {
    var index = this.infoGameCharacter.indexOf(character);
    
    console.log(this.infoGameCharacter);
    
    if(event.target.checked) {
      if(index < 0) {
        this.infoGameCharacter.push(character);
        console.log(character);
      }
    } else {
      if(index >= 0 ) {
        this.infoGameCharacter.splice(index, 1);
        console.log(index);
      }
    }
    
  }
  
  //
  // part 3 for the usergame part
  //
  platforms = [
      {
        name:'PC',
        games:[
          { 
            name: 'Left 4 Dead 2',
            character: ['Coach', 'Rochell', 'Nick', 'Ellis']
          },
          {
            name: 'World of Warcraft',
            character: ['Warrior', 'Paladin', 'Hunter', 'Rogue', 'Priest', 'Death Knight', 'haman', 'Mage', 'Warlock', 'Monk', 'Druid', 'Demon Hunter']
          }
        ]
      },
      {
        name:'PS3',
        games:[
          { 
            name: 'Destiny',
            character: ['Hunter', 'Warlock', 'Titan']
          },
          {
            name: 'Diablo 3',
            character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
          }
        ]
      },
      {
        name:'PS4',
        games:[
          { 
            name: 'Destiny',
            character: ['Hunter', 'Warlock', 'Titan']
          },
          {
            name: 'Diablo 3',
            character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
          }
        ]
      },
      {
        name:'XBox',
        games:[
          { 
            name: 'Destiny',
            character: ['Hunter', 'Warlock', 'Titan']
          },
          {
            name: 'Diablo 3',
            character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
          },
          { 
            name: 'Left 4 Dead 2',
            character: ['Coach', 'Rochell', 'Nick', 'Ellis']
          }
        ]
      },
      {
        name:'XB1',
        games:[
          { 
            name: 'Destiny',
            character: ['Hunter', 'Warlock', 'Titan']
          },
          {
            name: 'Diablo 3',
            character: ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
          }
        ]
      }
    ]
  wowCharacters =  ['Warrior', 'Paladin', 'Hunter', 'Rogue', 'Priest', 'Death Knight', 'haman', 'Mage', 'Warlock', 'Monk', 'Druid', 'Demon Hunter'];
  destinyCharacters = ['Hunter', 'Warlock', 'Titan'];
  D3Characters = ['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard'];
  L4D2Characters = ['Coach', 'Rochell', 'Nick', 'Ellis'];
    
  selectedPlatform: Platform;
  selectedGame: Game;
  
  onSelectPlatform(platform: Platform) {
    this.selectedPlatform = platform;
    console.log(this.selectedPlatform);
  }
  
  onSelectGame(game: Game){
    this.selectedGame = game;
    console.log(this.selectedGame);
  }

}
