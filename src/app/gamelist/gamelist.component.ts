import { Component, OnInit } from '@angular/core';

import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

import { ListPlayer } from '../model/ListPlayer';

@Component({
  selector: 'app-gamelist',
  templateUrl: './gamelist.component.html',
  styleUrls: ['./gamelist.component.css']
})
export class GamelistComponent implements OnInit {
  
  //game info stay untouched  
  games = [
  {
    name: 'World of Warcraft',
    id:1,
    platforms:['pc'],
    url:'http://cdn.wccftech.com/wp-content/uploads/2016/04/2364671-world-of-warcraft-logo.jpg',
    characters:['Warrior', 'Paladin', 'Hunter', 'Rogue', 'Priest', 'Death Knight', 'haman', 'Mage', 'Warlock', 'Monk', 'Druid', 'Demon Hunter']
  },
  {
    name: 'Diablo 3',
    id:2,
    platforms:['XBox', 'XBox One', 'PS3', 'PS4'],
    url:'https://static1.gamespot.com/uploads/original/1535/15354745/2722738-5353872158-255660.jpeg',
    characters:['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard']
  },
  {
    name: 'Left 4 Dead 2',
    id:3,
    platforms:['pc', 'XBox'],
    url:'http://cdn.wccftech.com/wp-content/uploads/2016/01/steamworkshop_webupload_previewfile_315734800_preview.png',
    characters:['Coach', 'Rochell', 'Nick', 'Ellis']
  },
  {
    name: 'Destiny',
    id:4,
    platforms:['XBox', 'XBox One', 'PS3', 'PS4'],
    url:'http://cdn2us.denofgeek.com/sites/denofgeekus/files/2016/09/destiny_1.jpg',
    characters:['Hunter', 'Warlock', 'Titan']
  }
    
  ]
  /*
  UNTOUCH
  */
  
  wowList: FirebaseListObservable<ListPlayer[]>;
  diabloList: FirebaseListObservable<ListPlayer[]>;
  l4d2List: FirebaseListObservable<ListPlayer[]>;
  destinyList: FirebaseListObservable<ListPlayer[]>;
  
  totalList: any[];
  constructor(private af: AngularFire) { }

  ngOnInit() {
    this.wowList = this.af.database.list('/players/game1');
    this.diabloList = this.af.database.list('/players/game2');
    this.l4d2List = this.af.database.list('/players/game3');
    this.destinyList = this.af.database.list('/players/game4');
  }

}
