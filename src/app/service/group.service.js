"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/toPromise');
var GroupService = (function () {
    /*    gamePosts = [
            {
             game: 'Destiny',
             type: 'match',
             startTime: new Date(2017, 2, 27, 12, 59, 59),
             endTime: Date(),
             description: 'Just have fun'
            },
            {
             game: 'Left 4 Dead 2',
             type: 'event',
             startTime:new Date(2017, 2, 27, 12, 59, 59),
             endTime: Date(),
             description: 'Best guys'
            },
            {
             game: 'World of WarCraft',
             type: 'event',
             startTime:new Date(2017, 2, 27, 12, 59, 59),
             endTime: Date(),
             description: 'for leisure'
            },
            {
             game: 'Destiny',
             type: 'event',
             startTime:new Date(2017, 2, 27, 12, 59, 59),
             endTime: Date(),
             description: 'FPS fans'
            }
        ]*/
    function GroupService(af) {
        this.af = af;
    }
    GroupService.prototype.getGamePosts = function (time) {
        if (time === void 0) { time = null; }
        //get now time
        var nowTime = (new Date()).getTime();
        // do not show the pasted game post
        this.gamePosts = this.af.database.list('/group', {
            query: {
                orderByChild: 'startTime',
                startAt: nowTime
            }
        });
        return this.gamePosts;
    };
    //add on a new post
    GroupService.prototype.addGamePost = function (newGamePost) {
        console.log(newGamePost);
        return this.gamePosts.push(newGamePost);
    };
    //delete a post
    GroupService.prototype.deleteGamePost = function (key) {
        return this.gamePosts.remove(key);
    };
    //get personal grouping post
    GroupService.prototype.getOwnGamePosts = function (userid) {
        console.log(userid);
        this.ownGamePosts = this.af.database.list('/group', {
            query: {
                orderByChild: 'uid',
                equalTo: userid
            }
        });
        return this.ownGamePosts;
    };
    //
    //edit the game post
    GroupService.prototype.editGamePost = function (key, updatePost) {
        console.log(updatePost);
        return this.ownGamePosts.update(key, updatePost);
    };
    GroupService = __decorate([
        core_1.Injectable()
    ], GroupService);
    return GroupService;
}());
exports.GroupService = GroupService;
