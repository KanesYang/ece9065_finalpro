import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import 'rxjs/add/operator/toPromise';

import { GamePost } from '../model/GamePost';

@Injectable()

export class GroupService{
    
/*    gamePosts = [
        {
         game: 'Destiny',
         type: 'match',
         startTime: new Date(2017, 2, 27, 12, 59, 59),
         endTime: Date(),
         description: 'Just have fun'
        },
        {
         game: 'Left 4 Dead 2',
         type: 'event',
         startTime:new Date(2017, 2, 27, 12, 59, 59),
         endTime: Date(),
         description: 'Best guys'
        },
        {
         game: 'World of WarCraft',
         type: 'event',
         startTime:new Date(2017, 2, 27, 12, 59, 59),
         endTime: Date(),
         description: 'for leisure'
        },
        {
         game: 'Destiny',
         type: 'event',
         startTime:new Date(2017, 2, 27, 12, 59, 59),
         endTime: Date(),
         description: 'FPS fans'
        }
    ]*/
    
    constructor(private af: AngularFire ){}
    
    gamePosts: FirebaseListObservable<GamePost[]>;
    
    getGamePosts( time: number = null) {
        //get now time
        var nowTime = (new Date()).getTime();

        // do not show the pasted game post
        this.gamePosts = this.af.database.list('/group', { 
                query: {
                    orderByChild: 'startTime',
                    startAt: nowTime
                }
        }) as FirebaseListObservable<GamePost[]>
        

        return this.gamePosts;
    }
    
    //add on a new post
    addGamePost(newGamePost) {
        console.log(newGamePost);
        return this.gamePosts.push(newGamePost);
    }
    
    //delete a post
    deleteGamePost(key) {
        return this.gamePosts.remove(key);
    }
    
    
    //own grouping post
    ownGamePosts: FirebaseListObservable<GamePost[]>;
    
    //get personal grouping post
    getOwnGamePosts(userid) {
        console.log(userid);
        this.ownGamePosts = this.af.database.list('/group', {
              query: {
                  orderByChild: 'uid',                    
                  equalTo: userid
              }
        }) as FirebaseListObservable<GamePost[]>
        
        return this.ownGamePosts;
    }
    
    //
    //edit the game post
    editGamePost(key, updatePost) {
        console.log(updatePost);
        return this.ownGamePosts.update(key, updatePost);
    }
    
}