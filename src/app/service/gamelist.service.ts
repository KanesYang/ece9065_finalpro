import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';

//import 'Listgame' from '../model/Listgame';

import { ListPlayer } from '../model/ListPlayer';

@Injectable()
 
export class GamelistService {
  
   currentPlayerList: FirebaseListObservable<ListPlayer[]>;
  
   constructor(private af: AngularFire) {
   }
   
    
    //game detail info
    games = [
      {
        name: 'World of Warcraft',
        id:1,
        platforms:['pc'],
        url:'http://cdn.wccftech.com/wp-content/uploads/2016/04/2364671-world-of-warcraft-logo.jpg',
        characters:['Warrior', 'Paladin', 'Hunter', 'Rogue', 'Priest', 'Death Knight', 'haman', 'Mage', 'Warlock', 'Monk', 'Druid', 'Demon Hunter'],
        description: "World of Warcraft (WoW) is a massively multiplayer online role-playing game (MMORPG) released in 2004 by Blizzard Entertainment. It is the fourth released game set in the fantasy Warcraft universe, which was first introduced by Warcraft: Orcs & Humans in 1994. World of Warcraft takes place within the Warcraft world of Azeroth, approximately four years after the events at the conclusion of Blizzard's previous Warcraft release, Warcraft III: The Frozen Throne. Blizzard Entertainment announced World of Warcraft on September 2, 2001. The game was released on November 23, 2004, on the 10th anniversary of the Warcraft franchise."
      },
      {
        name: 'Diablo 3',
        id:2,
        platforms:['XBox', 'XBox One', 'PS3', 'PS4'],
        url:'https://static1.gamespot.com/uploads/original/1535/15354745/2722738-5353872158-255660.jpeg',
        characters:['Barbarian', 'Crusader', 'Demon Hunter', 'Monk', 'Witch ', 'Doctor', 'Wizard'],
        description:"Diablo III is a hack and slash action role-playing video game developed and published by Blizzard Entertainment. It is the third installment in the Diablo franchise and was released in the Americas, Europe, South Korea, and Taiwan on May 15, 2012, and Russia on June 7, 2012, for Microsoft Windows and macOS. A console version was released for the PlayStation 3 and Xbox 360 on September 3, 2013. Versions for PlayStation 4 and Xbox One were released on August 19, 2014."
      },
      {
        name: 'Left 4 Dead 2',
        id:3,
        platforms:['pc', 'XBox'],
        url:'http://cdn.wccftech.com/wp-content/uploads/2016/01/steamworkshop_webupload_previewfile_315734800_preview.png',
        characters:['Coach', 'Rochell', 'Nick', 'Ellis'],
        description: "Left 4 Dead 2 is a cooperative first-person shooter video game developed and published by Valve Corporation. The sequel to Turtle Rock Studios's Left 4 Dead, the game released for Microsoft Windows and Xbox 360 in November 2009, and for OS X in October 2010, and for Linux in July 2013."
      },
      {
        name: 'Destiny',
        id:4,
        platforms:['XBox', 'XBox One', 'PS3', 'PS4'],
        url:'http://cdn2us.denofgeek.com/sites/denofgeekus/files/2016/09/destiny_1.jpg',
        characters:['Hunter', 'Warlock', 'Titan'],
        description: "Destiny is an online-only multiplayer first-person shooter video game developed by Bungie and published by Activision. It was released worldwide on September 9, 2014, for the PlayStation 3, PlayStation 4, Xbox 360, and Xbox One consoles. Destiny marked Bungie's first new console franchise since the Halo series, and it is the first game in a ten-year agreement between Bungie and Activision. Set in a mythic science fiction world, the game features a multiplayer shared-world environment with elements of role-playing games. Activities in Destiny are divided among player versus environment (PvE) and player versus player (PvP) game types. In addition to normal story missions, PvE features three-player strikes and six-player raids. A free roam patrol mode is also available for each planet and features public events. PvP features objective-based modes, as well as traditional deathmatch game modes."
      }
  ]    
  
    //whether the user name add or not
    addState = false;

    getGame(id: number) {
        return this.games[id-1];
    }
    
    getPlayerList(gameid) {
      this.currentPlayerList = this.af.database.list('/players/game' + gameid);
      this.af.database.list('/players/game' + gameid)
        .subscribe(
           list => console.log(list)
          )
      //console.log(this.currentPlayerList);
      return this.currentPlayerList;
    }
    
    addPlayer(newPlayer) {
      this.currentPlayerList.push(newPlayer)
    }
    
    getAddState() {
      return this.addState;
    }
    
    changeAddStatus(){
      this.addState = !this.addState;
      return this.addState;
    }
    
}