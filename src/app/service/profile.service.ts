import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import 'rxjs/add/operator/toPromise';
import { Observable, Subject } from "rxjs/Rx";

import * as firebase from 'firebase';

import { UserBasic } from '../model/UserBasic';
import { UserPlatform } from '../model/UserPlatform';
import { UserGame } from '../model/UserGame';

@Injectable()

export class ProfileService{ 
    
    folder: any;
    
    constructor(private af: AngularFire){
        this.folder = 'profileImages';
    }
    
    //Basic Profile part
    savedBasicProfile: FirebaseObjectObservable<UserBasic>;
    
    getBasicProfile(uid: string) {
        this.savedBasicProfile = this.af.database.object('/users/user'+uid);
        return this.savedBasicProfile;
    }
    
    addBasicProfile(basicProfile) {
        // create root ref
        let storageRef = firebase.storage().ref();
        
        console.log()
        if([(<HTMLInputElement>document.getElementById('image')).files[0]][0] !== undefined){
            //Check the size of the image, if too large, return false
            
            if( [(<HTMLInputElement>document.getElementById('image')).files[0]][0].size > 500000 ) {
                alert("Your image is too large! Change a smaller one");
                return false;
            }
            for(let selectedFile of [(<HTMLInputElement>document.getElementById('image')).files[0]]){
                console.log([(<HTMLInputElement>document.getElementById('image')).files[0]]);
                let path = `/${this.folder}/${selectedFile.name}`;
                let iRef = storageRef.child(path);
                iRef.put(selectedFile).then((snapshot) => {
                    basicProfile.image = selectedFile.name;
                    basicProfile.path = path;
                    //change set to update
                    return this.savedBasicProfile.update(basicProfile);
                });
            }
        } else {
            /*
            When there is no image
            */
            basicProfile.image = "photo.jpg";
            basicProfile.path = "/profileImages/photo.jpg";
            this.savedBasicProfile.update(basicProfile);
        }
    }
    
    
    //Platform Profile part
    savedPlatformProfile: FirebaseListObservable<UserPlatform[]>;
    
    getPlatformProfile(uid: string) {
        this.savedPlatformProfile = this.af.database.list('/users/user' + uid +'/platformInfo');
        return this.savedPlatformProfile;
    }
    
    addPlatformProfile(platformProfile){
        console.log(platformProfile);
        console.log(this.savedPlatformProfile);
        return this.savedPlatformProfile.push(platformProfile);
    }
    
    
    //game profile part
    savedGameProfile: FirebaseListObservable<UserGame[]>;
    
    getGameProfile(uid: string) {
        this.savedGameProfile = this.af.database.list('/users/user'+ uid + '/gameinfo');
        return this.savedGameProfile;
    }
    
    addGameProfile(gameProfile) {
        console.log(gameProfile);
        console.log(this.savedGameProfile);
        return this.savedGameProfile.push(gameProfile);
    }
}