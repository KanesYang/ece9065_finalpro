"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/toPromise');
var firebase = require('firebase');
var ProfileService = (function () {
    function ProfileService(af) {
        this.af = af;
        this.folder = 'profileImages';
    }
    ProfileService.prototype.getBasicProfile = function (uid) {
        this.savedBasicProfile = this.af.database.object('/users/user' + uid);
        return this.savedBasicProfile;
    };
    ProfileService.prototype.addBasicProfile = function (basicProfile) {
        var _this = this;
        // create root ref
        var storageRef = firebase.storage().ref();
        console.log();
        if ([document.getElementById('image').files[0]][0] !== undefined) {
            //Check the size of the image, if too large, return false
            if ([document.getElementById('image').files[0]][0].size > 500000) {
                alert("Your image is too large! Change a smaller one");
                return false;
            }
            var _loop_1 = function(selectedFile) {
                console.log([document.getElementById('image').files[0]]);
                var path = "/" + this_1.folder + "/" + selectedFile.name;
                var iRef = storageRef.child(path);
                iRef.put(selectedFile).then(function (snapshot) {
                    basicProfile.image = selectedFile.name;
                    basicProfile.path = path;
                    //change set to update
                    return _this.savedBasicProfile.update(basicProfile);
                });
            };
            var this_1 = this;
            for (var _i = 0, _a = [document.getElementById('image').files[0]]; _i < _a.length; _i++) {
                var selectedFile = _a[_i];
                _loop_1(selectedFile);
            }
        }
        else {
            /*
            When there is no image
            */
            basicProfile.image = "photo.jpg";
            basicProfile.path = "/profileImages/photo.jpg";
            this.savedBasicProfile.update(basicProfile);
        }
    };
    ProfileService.prototype.getPlatformProfile = function (uid) {
        this.savedPlatformProfile = this.af.database.list('/users/user' + uid + '/platformInfo');
        return this.savedPlatformProfile;
    };
    ProfileService.prototype.addPlatformProfile = function (platformProfile) {
        console.log(platformProfile);
        console.log(this.savedPlatformProfile);
        return this.savedPlatformProfile.push(platformProfile);
    };
    ProfileService.prototype.getGameProfile = function (uid) {
        this.savedGameProfile = this.af.database.list('/users/user' + uid + '/gameinfo');
        return this.savedGameProfile;
    };
    ProfileService.prototype.addGameProfile = function (gameProfile) {
        console.log(gameProfile);
        console.log(this.savedGameProfile);
        return this.savedGameProfile.push(gameProfile);
    };
    ProfileService = __decorate([
        core_1.Injectable()
    ], ProfileService);
    return ProfileService;
}());
exports.ProfileService = ProfileService;
