import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from "angularfire2/angularfire2";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs/Rx";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

import * as firebase from 'firebase';

// declare var firebase: any;

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private auth: AngularFireAuth, private router: Router) {}
    
    //get the user is auth or not
    canActivate(): Observable<boolean> {
      
      const subject = new Subject<boolean>();
      firebase.auth().onAuthStateChanged(function(user) {
          if(user) {
            subject.next(true);
          } else {
            subject.next(false);
          }
        });
      return subject.asObservable();
      
    }
    
    // get the user's email is verified or not
    emailVerified(): Observable<boolean> {
      
      const subject = new Subject<boolean>();
      firebase.auth().onAuthStateChanged(function(user) {
        if(user) {
          console.log(user);
          if(user.providerData[0].providerId == "facebook.com"){
            subject.next(true);
          } else {
            if(user.emailVerified) {
              //check whether verified the email
              console.log(user.emailVerified);
              subject.next(true);
            } else {
              subject.next(false);
            }
          }
        }
      });
      
      return subject.asObservable();
    }
    
    //return user's uid;
    getUid() {
      var user = firebase.auth().currentUser;
      console.log(user);
      if(user) {
        return user.uid;
      } else {
        return undefined;
      }
    }
    
    
    
  
}



    

