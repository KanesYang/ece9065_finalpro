"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
require('rxjs/add/operator/do');
require('rxjs/add/operator/map');
require('rxjs/add/operator/take');
var firebase = require('firebase');
// declare var firebase: any;
var AuthGuard = (function () {
    function AuthGuard(auth, router) {
        this.auth = auth;
        this.router = router;
    }
    //get the user is auth or not
    AuthGuard.prototype.canActivate = function () {
        var subject = new Rx_1.Subject();
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                subject.next(true);
            }
            else {
                subject.next(false);
            }
        });
        return subject.asObservable();
    };
    // get the user's email is verified or not
    AuthGuard.prototype.emailVerified = function () {
        var subject = new Rx_1.Subject();
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                console.log(user);
                if (user.providerData[0].providerId == "facebook.com") {
                    subject.next(true);
                }
                else {
                    if (user.emailVerified) {
                        //check whether verified the email
                        console.log(user.emailVerified);
                        subject.next(true);
                    }
                    else {
                        subject.next(false);
                    }
                }
            }
        });
        return subject.asObservable();
    };
    //return user's uid;
    AuthGuard.prototype.getUid = function () {
        var user = firebase.auth().currentUser;
        console.log(user);
        if (user) {
            return user.uid;
        }
        else {
            return undefined;
        }
    };
    AuthGuard = __decorate([
        core_1.Injectable()
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;
