"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_1 = require('@angular/platform-browser');
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var angularfire2_1 = require('angularfire2');
var app_component_1 = require('./app.component');
var login_component_1 = require('./login/login.component');
var email_component_1 = require('./email/email.component');
var signup_component_1 = require('./signup/signup.component');
var members_component_1 = require('./members/members.component');
var auth_service_1 = require('./service/auth.service');
var gamelist_service_1 = require('./service/gamelist.service');
var group_service_1 = require('./service/group.service');
var profile_service_1 = require('./service/profile.service');
var app_routes_1 = require('./app.routes');
var forgot_password_component_1 = require('./forgot-password/forgot-password.component');
var header_component_1 = require('./header/header.component');
var frontpage_component_1 = require('./frontpage/frontpage.component');
var profilecreate_component_1 = require('./profilecreate/profilecreate.component');
var gamelist_component_1 = require('./gamelist/gamelist.component');
var gamedetail_component_1 = require('./gamedetail/gamedetail.component');
var grouping_component_1 = require('./grouping/grouping.component');
exports.firebaseConfig = {
    apiKey: "AIzaSyACQNfVzTxrDUaqQMS7MyPc2z5Bl8En12w",
    authDomain: "matchgame-f33fe.firebaseapp.com",
    databaseURL: "https://matchgame-f33fe.firebaseio.com",
    storageBucket: "matchgame-f33fe.appspot.com",
    messagingSenderId: "800222234508"
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                email_component_1.EmailComponent,
                signup_component_1.SignupComponent,
                members_component_1.MembersComponent,
                forgot_password_component_1.ForgotPasswordComponent,
                header_component_1.HeaderComponent,
                frontpage_component_1.FrontpageComponent,
                profilecreate_component_1.ProfilecreateComponent,
                gamelist_component_1.GamelistComponent,
                gamedetail_component_1.GamedetailComponent,
                grouping_component_1.GroupingComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                angularfire2_1.AngularFireModule.initializeApp(exports.firebaseConfig),
                app_routes_1.routes
            ],
            providers: [auth_service_1.AuthGuard, gamelist_service_1.GamelistService, group_service_1.GroupService, profile_service_1.ProfileService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
