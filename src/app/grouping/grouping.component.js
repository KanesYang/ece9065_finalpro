"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
require('rxjs/add/operator/switchMap');
var GroupingComponent = (function () {
    function GroupingComponent(authService, groupService) {
        var _this = this;
        this.authService = authService;
        this.groupService = groupService;
        this.authService.canActivate().subscribe(function (authStatus) {
            _this.isAuthenticated = authStatus;
            console.log(_this.isAuthenticated);
        });
    }
    GroupingComponent.prototype.ngOnInit = function () {
        var _this = this;
        //default app state
        this.addState = false;
        this.editState = false;
        //get the uid
        //IMPORTANT 	 			        
        this.uid = this.authService.getUid();
        console.log(this.uid);
        //get now time
        //use for check if it is editable
        this.nowTime = (new Date()).getTime();
        //get all the grouping post
        this.groupService.getGamePosts()
            .subscribe(function (gamePosts) {
            _this.gamePosts = gamePosts;
            _this.originPosts = gamePosts;
            console.log(_this.gamePosts);
        });
        //get personal grouping post
        this.groupService.getOwnGamePosts(this.uid)
            .subscribe(function (gamePosts) {
            _this.ownGamePosts = gamePosts;
            //show only the past
            _this.ownGamePosts = _this.ownGamePosts.filter(function (item) {
                return item.endTime < _this.nowTime;
            });
            console.log(_this.ownGamePosts);
        });
        console.log(this.platformFilter);
        console.log(this.gameFilter);
        console.log(this.timeFilter);
        console.log(this.gameFilter == undefined && this.platformFilter == undefined && this.timeFilter == undefined);
    };
    /*
      end of ngOnInit
    */
    GroupingComponent.prototype.isAuth = function () {
        return this.isAuthenticated;
    };
    //decide to show the add form or not
    GroupingComponent.prototype.changeAddState = function () {
        this.addState = !this.addState;
        this.editState = false;
    };
    /*
    part for filter function
    platform:string = null, game:string = null,
    */
    GroupingComponent.prototype.applyFilter = function (platform, game, time) {
        var _this = this;
        if (platform === void 0) { platform = null; }
        if (game === void 0) { game = null; }
        if (time === void 0) { time = null; }
        //origin post is set up the filter function
        this.gamePosts = this.originPosts;
        this.platformFilter = platform;
        this.gameFilter = game;
        this.timeFilter = this.dateProcess(time);
        //console.log(this.gameFilter == undefined && this.platformFilter == undefined  && this.timeFilter == undefined);
        //apply the filter 
        // use filter()
        this.gamePosts = this.gamePosts
            .filter(function (item) {
            if (_this.gameFilter == '') {
                return true;
            }
            // console.log("not empty 1");
            // console.log(item.game);
            return item.game == _this.gameFilter;
        })
            .filter(function (item) {
            if (_this.platformFilter == '') {
                return true;
            }
            return item.platform == _this.platformFilter;
        })
            .filter(function (item) {
            if (isNaN(_this.timeFilter)) {
                return true;
            }
            return item.endTime > _this.timeFilter;
        });
    };
    //transfer the time into the millisecond
    GroupingComponent.prototype.dateProcess = function (dateTime) {
        return (new Date(dateTime)).getTime();
    };
    /*
    part for add the new post
    */
    GroupingComponent.prototype.addGamePost = function (game, type, platform, startTime, endTime, description) {
        var modStartTime = (new Date(startTime)).getTime();
        var modEndTime = (new Date(endTime)).getTime();
        var newGamePost = {
            game: game,
            type: type,
            platform: platform,
            startTime: modStartTime,
            endTime: modEndTime,
            description: description,
            uid: this.uid
        };
        console.log(newGamePost);
        this.groupService.addGamePost(newGamePost);
    };
    GroupingComponent.prototype.onSelectPlatform = function (platform) {
        this.selectedPlatform = platform;
        console.log(this.selectedPlatform);
    };
    /*
    delete a form post
    */
    GroupingComponent.prototype.deleteGamePost = function (key) {
        this.groupService.deleteGamePost(key);
    };
    //shows edit panel
    GroupingComponent.prototype.showEdit = function (gamePost, key) {
        this.addState = false;
        this.editState = true;
        this.activeKey = gamePost.$key;
        this.activePlatform = gamePost.platform;
        this.activeType = gamePost.type;
        this.activeGame = gamePost.game;
        this.activeStartTime = gamePost.startTime;
        this.activeEndTime = gamePost.endTime;
        this.activeDescription = gamePost.Description;
    };
    //update the past game post
    GroupingComponent.prototype.editGamePost = function (activeStartTime, activeEndTime) {
        var modStartTime = (new Date(activeStartTime)).getTime();
        var modEndTime = (new Date(activeEndTime)).getTime();
        var updatePost = {
            platform: this.activePlatform,
            type: this.activeType,
            game: this.activeGame,
            startTime: modStartTime,
            endTime: modEndTime,
            description: this.activeDescription
        };
        console.log(updatePost);
        this.groupService.editGamePost(this.activeKey, updatePost);
        this.editState = false;
    };
    GroupingComponent = __decorate([
        core_1.Component({
            selector: 'app-grouping',
            templateUrl: './grouping.component.html',
            styleUrls: ['./grouping.component.css']
        })
    ], GroupingComponent);
    return GroupingComponent;
}());
exports.GroupingComponent = GroupingComponent;
