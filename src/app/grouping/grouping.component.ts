import { Component, Input, OnInit  } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';

import * as firebase from 'firebase';

import { AuthGuard } from '../service/auth.service';
import { GroupService } from '../service/group.service';

import { GamePost } from '../model/GamePost';

@Component({
  selector: 'app-grouping',
  templateUrl: './grouping.component.html',
  styleUrls: ['./grouping.component.css']
})
export class GroupingComponent implements OnInit {
  
  //check the user login or not
  isAuthenticated;
  
  //get the current user uid
  uid: string;
  
  //get now time
  nowTime;
  
  //whether to show the add form 
  addState: boolean;
  user: any;
  gamePosts: GamePost[];
  ownGamePosts: GamePost[];
  
  //origin post is set up the filter function
  originPosts: GamePost[];
  
  constructor(private authService: AuthGuard,
              private groupService: GroupService) {

    this.authService.canActivate().subscribe(
      authStatus => {
        this.isAuthenticated = authStatus;
        console.log(this.isAuthenticated)
      });                
  }

  ngOnInit() {
    //default app state
    this.addState = false;
    this.editState = false;
    
    //get the uid
    //IMPORTANT 	 			        
	 	this.uid = this.authService.getUid();
    console.log(this.uid);
    
    //get now time
    //use for check if it is editable
    this.nowTime = (new Date()).getTime();
    
    //get all the grouping post
    this.groupService.getGamePosts()
        .subscribe(gamePosts => {
          this.gamePosts = gamePosts;
          this.originPosts = gamePosts;
          console.log(this.gamePosts);
        });
        
    //get personal grouping post
    this.groupService.getOwnGamePosts(this.uid)
      .subscribe( gamePosts => {
       this.ownGamePosts = gamePosts;
       //show only the past
       this.ownGamePosts = this.ownGamePosts.filter(
          (item) => {
            return item.endTime < this.nowTime;
          }
         );
       
      
       console.log(this.ownGamePosts);
      });
    
    console.log(this.platformFilter);
    console.log(this.gameFilter);
    console.log(this.timeFilter);
    console.log(this.gameFilter == undefined && this.platformFilter == undefined  && this.timeFilter == undefined);
  }
  
  /*
    end of ngOnInit
  */
  
  isAuth() {
    return this.isAuthenticated;
  }
  
  //decide to show the add form or not
  changeAddState() {
    this.addState = !this.addState;
    this.editState = false;
  }
  
  /*
  part for filter function
  platform:string = null, game:string = null, 
  */
  applyFilter(platform:string = null, game:string = null, time:string = null) {
    
    //origin post is set up the filter function
    this.gamePosts = this.originPosts;
    
    this.platformFilter = platform;
    this.gameFilter = game;
    this.timeFilter = this.dateProcess(time);
    
    //console.log(this.gameFilter == undefined && this.platformFilter == undefined  && this.timeFilter == undefined);
    
    //apply the filter 
    // use filter()
    this.gamePosts =this.gamePosts
                      .filter(
                        (item) => {
                          if(this.gameFilter == '') {return true;} 
                            // console.log("not empty 1");
                            // console.log(item.game);
                            return item.game == this.gameFilter;
                        }
                      )
                      .filter(
                        (item) => {
                          if(this.platformFilter == ''){return true;}
                          return item.platform == this.platformFilter;
                        }
                      )
                      .filter(
                        (item) => {
                          if(isNaN(this.timeFilter)) { return true; } 
                          return item.endTime > this.timeFilter;
                        }
                      )
  
  
  }
  
  //use for hidden
  platformFilter: string;
  gameFilter: string;
  timeFilter: number;
  
  //transfer the time into the millisecond
  dateProcess(dateTime: string) {
    return (new Date(dateTime)).getTime();
  }



  /*
  part for add the new post
  */
  addGamePost(
    game: string,
    type: string,
    platform: string,
    startTime: string,
    endTime: string,
    description: string
    ) {
    
    var modStartTime = (new Date(startTime)).getTime();
    var modEndTime = (new Date(endTime)).getTime();
    
    var newGamePost = {
      game: game,
      type: type,
      platform: platform,
      startTime: modStartTime,
      endTime: modEndTime,
      description: description,
      uid: this.uid
    }
    
    console.log(newGamePost);
    
    this.groupService.addGamePost(newGamePost);
  }
  
  
  
  // part to show the related platform for the add post part
  selectedPlatform;
  
  onSelectPlatform(platform){
    this.selectedPlatform = platform;
    console.log(this.selectedPlatform);
  }
  
  
  
  /*
  delete a form post
  */
  deleteGamePost(key) {
    this.groupService.deleteGamePost(key);
  }
  
  
  /*
  Start Edit a form post
  */
  editState: boolean;
  
  //activekey to identify which item to update
  activeKey: string;
  //things show on the edit panel
  activePlatform: string;
  activeType: string;
  activeGame: string;
  activeStartTime: any;
  activeEndTime: any;
  activeDescription: string;
  
  
  //shows edit panel
  showEdit(gamePost, key){
    this.addState = false;
    this.editState = true;
    
    
    this.activeKey = gamePost.$key;
    
    this.activePlatform = gamePost.platform;
    this.activeType = gamePost.type;
    this.activeGame = gamePost.game;
    this.activeStartTime = gamePost.startTime;
    this.activeEndTime = gamePost.endTime;
    this.activeDescription = gamePost.Description;
  }
  
  
  //update the past game post
  editGamePost(activeStartTime, activeEndTime) {
    
    var modStartTime = (new Date(activeStartTime)).getTime();
    var modEndTime = (new Date(activeEndTime)).getTime();
    
    let updatePost = {
      platform: this.activePlatform,
      type: this.activeType,
      game: this.activeGame,
      startTime: modStartTime,
      endTime: modEndTime,
      description: this.activeDescription
    }
    
    console.log(updatePost);
    
    this.groupService.editGamePost(this.activeKey, updatePost);
    
    this.editState = false;
  }
  
  /*
  End  Edit a form post
  */
}
