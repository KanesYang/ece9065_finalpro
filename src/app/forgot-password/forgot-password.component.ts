import { Component, OnInit } from '@angular/core';
import { AngularFire } from 'angularfire2';
import * as firebase from 'firebase'

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(private af: AngularFire) { }
  
  resetPassword(emailAddress) {
    firebase.auth().sendPasswordResetEmail(emailAddress).then(function() {
      // Email sent.
      console.log("success");
    }, function(error) {
      // An error happened.
      console.log("error" + error);
    });

  }
    
  ngOnInit() {
    console.log(this.af);
  }

}
