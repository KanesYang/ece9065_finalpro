"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var firebase = require('firebase');
var HeaderComponent = (function () {
    function HeaderComponent(authService, af, router) {
        var _this = this;
        this.authService = authService;
        this.af = af;
        this.router = router;
        this.authService.canActivate().subscribe(function (authStatus) {
            _this.isAuthenticated = authStatus;
            // console.log(this.isAuthenticated);
        });
        /*
          check the email is verified or not
        */
        this.authService.emailVerified().subscribe(function (emailStatus) {
            _this.isVerified = emailStatus;
            // console.log(this.isVerified);
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        console.log(this.isAuthenticated);
        //fix the problem that the facebook account cannot do the email verified
        if (this.isAuthenticated) {
            console.log("check?");
            if (firebase.auth().currentUser.providerData[0].providerId == "facebook.com") {
                this.isVerified = true;
            }
        }
    };
    HeaderComponent.prototype.isAuth = function () {
        return this.isAuthenticated;
    };
    HeaderComponent.prototype.emailVerify = function () {
        return this.isVerified;
    };
    HeaderComponent.prototype.onLogout = function () {
        this.af.auth.logout();
        console.log('logged out');
        this.router.navigateByUrl('/login');
    };
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            templateUrl: './header.component.html',
            styleUrls: ['./header.component.css']
        })
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;
