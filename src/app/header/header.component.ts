import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from "rxjs/Rx";
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import * as firebase from 'firebase';

import { AuthGuard } from '../service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  isAuthenticated;
  isVerified: boolean;

  constructor(private authService: AuthGuard,
              private af: AngularFire,
              private router: Router) { 
    this.authService.canActivate().subscribe(
      authStatus => {
        this.isAuthenticated = authStatus;
       // console.log(this.isAuthenticated);
      });
      
    /*
      check the email is verified or not
    */
    
    this.authService.emailVerified().subscribe(
      emailStatus => {
        this.isVerified = emailStatus;
       // console.log(this.isVerified);
      })

  }

  ngOnInit() {
    console.log(this.isAuthenticated);
    //fix the problem that the facebook account cannot do the email verified
    if(this.isAuthenticated){
      console.log("check?")
      if(firebase.auth().currentUser.providerData[0].providerId == "facebook.com"){
        this.isVerified = true;
      }
    }
  }
  
  isAuth() {
    return this.isAuthenticated;
  }
  
  emailVerify(){
    return this.isVerified;
  }
  
  onLogout() {
     this.af.auth.logout();
     console.log('logged out');
     this.router.navigateByUrl('/login');
  }

}
