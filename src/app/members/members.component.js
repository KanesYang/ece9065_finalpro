"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var MembersComponent = (function () {
    function MembersComponent(af, authService) {
        var _this = this;
        this.af = af;
        this.authService = authService;
        this.state = '';
        console.log(this.af);
        this.af.auth.subscribe(function (auth) {
            if (auth) {
                _this.name = auth;
            }
        });
    }
    MembersComponent.prototype.ngOnInit = function () {
        this.userCount = this.af.database.list('/users');
        this.uid = this.authService.getUid();
        this.currentUser = this.af.database.object('/users/user' + this.uid);
        console.log(this.currentUser);
    };
    MembersComponent = __decorate([
        core_1.Component({
            selector: 'app-members',
            templateUrl: './members.component.html',
            styleUrls: ['./members.component.css']
        })
    ], MembersComponent);
    return MembersComponent;
}());
exports.MembersComponent = MembersComponent;
