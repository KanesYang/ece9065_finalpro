import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Router } from '@angular/router';

import { AuthGuard } from '../service/auth.service';

import * as firebase from 'firebase';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})


export class MembersComponent implements OnInit {
  name: any;
  state: string = '';
  uid: string;
  
  userCount: FirebaseListObservable<any[]>;
  currentUser: FirebaseObjectObservable<any>;
 
  constructor(public af: AngularFire,private authService: AuthGuard ) {
    console.log(this.af);
    this.af.auth.subscribe(auth => {
      if(auth) {
        this.name = auth;
      }
    });
    

  }

  ngOnInit() {
    this.userCount = this.af.database.list('/users');
    this.uid = this.authService.getUid();
    this.currentUser = this.af.database.object('/users/user'+this.uid);
    console.log(this.currentUser);
  }

}
