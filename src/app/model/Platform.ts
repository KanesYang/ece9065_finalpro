import { Game } from './Game';

export interface Platform {
    name: string;
    games: Game[];
}