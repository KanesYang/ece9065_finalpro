export interface Listgame {
    name: string;
    id: number;
    platforms: string[];
    url: string;
    characters: string[];
    description: string;
}