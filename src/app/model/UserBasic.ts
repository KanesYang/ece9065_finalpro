export interface UserBasic {
    uid: string;
    username: string;
    image: string;
    age: number;
    startTime: number;
    endTime: number;
}