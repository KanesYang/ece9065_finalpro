export interface UserGame{
    uid: string;
    infoGamePlatform?: string;
    infoGameId?: string;
    infoChannel?: boolean;
    infoGameName?:string;
    infoGameStyle?: string;
    infoGameCharacter?: string[];
}