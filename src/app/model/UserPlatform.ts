export interface UserPlatform{
    uid: string;
    infoPlatform: string;
    infoGameId: string;
    infoChannel: boolean;
}