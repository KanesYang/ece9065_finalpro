export interface GamePost {
    $key?: string;
    uid: string; 
    game: string;
    platform: string;
    type: number;
    startTime: number;
    endTime: number;
    description: string;
}

// $key?: string   ?=>optional
// *ngIf ="!(af.auth | async)" 