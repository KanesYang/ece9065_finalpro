import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { MembersComponent } from './members/members.component';
import { AuthGuard } from './service/auth.service';
import { SignupComponent } from './signup/signup.component';
import { EmailComponent } from './email/email.component';
import { FrontpageComponent } from './frontpage/frontpage.component';
import { ProfilecreateComponent } from './profilecreate/profilecreate.component';
import { GamelistComponent } from './gamelist/gamelist.component';
import { GamedetailComponent } from './gamedetail/gamedetail.component';
import { GroupingComponent } from './grouping/grouping.component';


export const router: Routes = [
    { path: '', redirectTo: 'frontpage', pathMatch: 'full' },
    { path: 'frontpage', component: FrontpageComponent},
    { path: 'login', component: LoginComponent },
    { path: 'forgot', component:ForgotPasswordComponent},
    { path: 'signup', component: SignupComponent },
    { path: 'login-email', component: EmailComponent },
    { path: 'gamelist', component: GamelistComponent},
    { path: 'lookingforgroup', component: GroupingComponent},
    { path: 'gamedetail/:id', component: GamedetailComponent},
    { path: 'members', component: MembersComponent, canActivate: [AuthGuard] },
    { path: 'profilecreate', component: ProfilecreateComponent, canActivate: [AuthGuard] }

]

export const routes: ModuleWithProviders = RouterModule.forRoot(router);