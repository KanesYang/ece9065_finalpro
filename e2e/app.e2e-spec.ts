import { MatchgamePage } from './app.po';

describe('matchgame App', () => {
  let page: MatchgamePage;

  beforeEach(() => {
    page = new MatchgamePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
